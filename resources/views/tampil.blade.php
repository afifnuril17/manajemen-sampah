<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <style>

    </style>
</head>

<body id="tampil">
    <section class="header" id="header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="text-center">Data Sampah</h2>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <form>
                        <div class="form-grup btn_tambah">
                            <button class="btn form-control" type="submit">Tambah</button>
                        </div>
                        <div id="kategori"></div>
                        <!-- <div class="card-body ">
                            <h5 id="kategori_detail">' + data.nama + '</h5>
                            <h6 class="text-right"><a href="">Hapus</a></h6>
                        </div> -->
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="js/jquery-3.5.1.min.js"></script>
    <script src="js/popper.min.js"></script>


    <script>
        let content = '';
        let content_title = '';
        $.getJSON('data.json', function(data) {
            let data_kategori = data.data;



            $.each(data_kategori, function(i, data) {


                content = '<p class = "tambahan_left">' + data.kategori + '</p>';
                let nama_kategori = data.data_kategori;
                $.each(nama_kategori, function(i, data) {
                    content_title = '<div><b><p>' + data.nama + '<p></b></div>';
                    $('#kategori').append('<div class="card"><div class="card-body">' + content_title + content + '</><a class="tambahan" href="">Hapus</a></div></diV>');
                });

            })
        });
    </script>
</body>

</html>
