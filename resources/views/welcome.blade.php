<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body>
    <section class="header" id="header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="text-center">Input Sampah</h2>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col align-self-center">
                    <form>

                        <div class="form-group">
                            <label for="nama">Kategori Sampah</label>
                            <select class="form-control" id="kategori">
                                <!-- <option>Kategori</option>
                                <option>Kertas</option>
                                <option>Plastik</option>
                                <option>Logam</option> -->
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nama">Nama Sampah</label>
                            <input type="text" class="form-control" placeholder="Nama Sampah">
                        </div>
                        <div class="form-grup btn_tambah">
                            <button class="btn form-control" type="submit">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script src="js/jquery-3.5.1.min.js"></script>
    <script src="js/popper.min.js"></script>


    <script>
        $.getJSON('data.json', function(data) {
            let data_kategori = data.data;
            $.each(data_kategori, function(i, data) {
                $('#kategori').append('<option >' + data.kategori + ' </option>');
            })
        });
    </script>


</body>

</html>
