<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">



    <style>
        * {
            margin: 0;
        }

        body {
            background-color: #f5f5f5;
        }

        .kiri {
            background-color: #192a3f;
            height: 900px;
            width: 20%;
            float: left;
            position: fixed;
            color: white;

        }

        .kiri_atas {
            background-color: #30aee4;
            width: 100%;
            height: 80px;
        }

        .kiri_atas img {
            margin-top: 20px;
            width: 100px;
        }


        .kanan {
            width: 80%;
            float: right;
            background-color: #efefef;
        }

        .kanan_atas {
            background-color: white;
            width: 100%;
            height: 80px;
        }

        .kanan_atas i {
            height: 50px;
            padding-bottom: 25px;

        }

        .kanan_atas a {
            color: black;

        }

        .tombol {
            margin-left: 20px;
            background-color: transparent;
            border: none;
            margin-top: 15px;
            font-size: 30px;
        }

        table {
            background-color: white;
        }

        .content {
            padding-left: 20px;
            padding-right: 20px;
        }

        .j {
            box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.1);
        }

        .content h5 {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .navigasi {
            color: #81878e;
            padding-top: 20px;
            padding-left: 20px;
        }

        .content .a {
            padding-top: 20px;
            padding-bottom: 50px;
        }

        .content .a button {
            background-color: #30aee4;
            color: white;
        }

        .sampah:hover {
            border-left: 2px solid #30aee4;
        }

        .sorting {
            background-color: transparent;
            border: none;
        }
    </style>
</head>

<body>

    <div>

        <div class="kiri" id="hiden">
            <div class="kiri_atas">
                <center><img src="img/logo.png"></center>
            </div>
            <div>
                <p class="navigasi">Navigasi</p>
            </div>

            <div class="sampah">
                <button style="background-color:transparent; border:none; color:white; width:100%" onclick="tampilSemua()">
                    <p style="padding-left: 20px; padding-top:10px; float:left;">Sampah</p>
                    <i class="fa fa-angle-down" style="font-size: 30px; float: right; padding-right:20px; padding-top:10px;" aria-hidden="true"></i>
                </button>
            </div>
            <div class="sampah" style="display: none;">
                <button style="background-color:transparent; border:none; color:white; width:100%" onclick="sembunyiSemua()">
                    <p style="padding-left: 20px; padding-top:10px; float:left;">Sampah</p>
                    <i class="fa fa-angle-up" style="font-size: 30px; float: right; padding-right:20px; padding-top:10px;" aria-hidden="true"></i>
                </button>
            </div>

            <div id="semuatampil" style="display: none;">
                <div>
                    <button style="background-color:transparent; border:none; color: #81878e; width:100%" onclick="tampilSemua()">
                        <i class="fa fa-circle-o" aria-hidden="true" style="float: left; padding-top:15px; padding-left: 20px; font-size:10px"></i>
                        <p style="padding-left: 20px; padding-top:10px; float:left;">Tambah Sampah</p>
                    </button>

                </div>

                <div>
                    <button style="background-color:transparent; border:none; color: white; width:100%" onclick="tampilSemua()">
                        <i class="fa fa-circle" aria-hidden="true" style="float: left; padding-top:15px; padding-left: 20px; font-size:10px"></i>
                        <p style="padding-left: 20px; padding-top:10px; float:left;">Semua Sampah</p>
                    </button>
                </div>
            </div>

        </div>

        <div class="kanan" id="full">
            <div class="kanan_atas">
                <button class="tombol" onclick="sembunyi()"><i class="fa fa-bars" aria-hidden="true"></i></button>
                <button class="tombol" onclick="muncul()" style="display: none;"><i class="fa fa-bars" aria-hidden="true"></i></button>
            </div>

            <div class="content j">
                <h5>SAMPAH</h5>
            </div>
            <div class="content">
                <div class="a">
                    <p style="float: left;">Semua Sampah</p>
                    <button style="float: right;" type="button" class="btn" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat"><i class="fa fa-plus-circle" aria-hidden="true"></i> Tambah</button>

                </div>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <p class="modal-title" id="exampleModalLabel">TAMBAH SAMPAH</p>
                                <button type="button" onclick="modal()" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body" id="modal">
                                <div class="form-group">
                                    <label for="nama">Nama Sampah</label>
                                    <input type="text" class="form-control" placeholder="Nama Sampah">
                                </div>
                                <div class="form-group">
                                    <select class="form-control" id="kategori">
                                        <option>Kategori</option>
                                        <option>Kertas</option>
                                        <option>Plastik</option>
                                        <option>Logam</option>
                                    </select>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn" style="background-color: #e5e5e5;" data-dismiss="modal">Batal</button>
                                <button type="button" onclick="btn_tambah()" class="btn" style="background-color: #30aee4;">Tambah</button>
                            </div>
                        </div>
                    </div>
                </div>

                <table class="table">
                    <thead>
                        <tr style="background-color: #e2e2e2;">
                            <th scope="col">#</th>
                            <th scope="col" style="width: 300px;">Nama Sampah <span style="font-weight: normal; font-size: small; color: #c5c5c5"><button class="sorting" onclick="sortingNama()">&#8645;</button></span></th>
                            <th scope="col">Jenis Sampah <span style="font-weight: normal; font-size: small; color: #c5c5c5"><button onclick="sortingJenis()" class="sorting">&#8645;</button></span></th>
                            <th scope="col" style="float: right;">Aksi</th>
                        </tr>
                        <tr>
                            <th scope="row"></th>
                            <td>

                                <input type="text" class="form-control col-9" placeholder="Cari">

                            </td>
                            <td>

                                <input type="text" class="form-control col-5" placeholder="Cari">

                            </td>
                            <td></td>
                        </tr>
                    </thead>

                    <tbody id="kat"></tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $('#exampleModal').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.data('whatever') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text('New message to ' + recipient)
            modal.find('.modal-body input').val(recipient)
        })
    </script>


    <script src="js/jquery-3.5.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src=" js/fontawesome.js"> </script>
    <script src=" js/bootstrap.min.js"> </script>


    <script>
        function sembunyi() {
            document.getElementById('hiden').style = "display:none";
            document.getElementById('full').style = "width:100%;";
            document.getElementsByClassName('tombol')[0].style = "display:none";
            document.getElementsByClassName('tombol')[1].style = "display:block";
        }

        function muncul() {
            document.getElementById('hiden').style = "display:block";
            document.getElementById('full').style = "width:80%;";
            document.getElementsByClassName('tombol')[0].style = "display:block";
            document.getElementsByClassName('tombol')[1].style = "display:none";
        }

        function tampilSemua() {
            document.getElementById('semuatampil').style = "display:block";

            document.getElementsByClassName('sampah')[0].style = "display:none";
            document.getElementsByClassName('sampah')[1].style = "display:block";

        }


        function sembunyiSemua() {
            document.getElementById('semuatampil').style = "display:none";

            document.getElementsByClassName('sampah')[0].style = "display:block";
            document.getElementsByClassName('sampah')[1].style = "display:none";

        }
        let content = '';
        let content_title = '';
        let j = 1;
        $.getJSON('data.json', function(data) {

            let data_kategori = data.data;



            $.each(data_kategori, function(i, data, ) {

                content = '<td>' + data.kategori + '</td>';

                let nama_kategori = data.data_kategori;
                $.each(nama_kategori, function(i, data) {
                    content_title = '<td>' + data.nama + '</td>';
                    $('#kat').append('<tr><th scope="row">' + j++ + '</th>' + content_title + content + '<td><a class="tambahan" href="">Hapus</a></td></tr>');
                });

            })
        });

        function sortingJenis() {
            let content = '';
            let content_title = '';
            let j = 1;
            $.getJSON('data.json', function(data) {

                let data_kategori = data.data;



                $.each(data_kategori, function(i, data, ) {
                    console.log(data.kategori);
                    var x = [data.kategori];
                    x.concat(++x)
                    console.log(x);
                    content = '<td>' + data.kategori + '</td>';

                    let nama_kategori = data.data_kategori;

                    $.each(nama_kategori, function(i, data) {

                        content_title = '<td>' + data.nama + '</td>';

                        // $('#kat').append('<tr><th scope="row">' + j++ + '</th>' + content_title + content + '<td><a class="tambahan" href="">Hapus</a></td></tr>');
                    });

                })
            });
        }

        function btn_tambah() {
            $.getJSON('data.json', function(data) {

                }

            )
        };
    </script>

</body>

</html>
