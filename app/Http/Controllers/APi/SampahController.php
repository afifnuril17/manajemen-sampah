<?php

namespace App\Http\Controllers\APi;

use App\Http\Controllers\Controller;
use App\Sampah;
use Illuminate\Http\Request;
use App\Http\Resources\SampahItem;
use App\Http\Resources\SampahCollection;

class SampahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new SampahCollection(Sampah::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Sampah::rules(false));
        if (!Sampah::create($request->all())) {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        } else {
            return [
                'message' => 'OK',
                'code' => 200,
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sampah  $sampah
     * @return \Illuminate\Http\Response
     */
    public function show(Sampah $sampah)
    {
        return new SampahItem($sampah);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sampah  $sampah
     * @return \Illuminate\Http\Response
     */
    public function edit(Sampah $sampah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sampah  $sampah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sampah $sampah)
    {
        $this->validate($request, Sampah::rules(true, $sampah->id));
        if (!$sampah->update($request->all())) {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        } else {
            return [
                'message' => 'OK',
                'code' => 201,
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sampah  $sampah
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sampah $sampah)
    {
        if ($sampah->delete()) {
            return [
                'message' => 'OK',
                'code' => 204,
            ];
        } else {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        }
    }
}
