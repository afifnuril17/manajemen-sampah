<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sampah extends Model
{
    protected $guarded = ['id'];

    public static function rules($update = false, $id = null)
    {
        $rules = [
            'nama_sampah'    => 'required',
            'jenis_sampah'  => 'required',

        ];
        if ($update) {
            return $rules;
        };
    }
}
